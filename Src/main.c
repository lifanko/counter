
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2020 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */

#include "tm1638.h"
#include "vl53l0x.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

uint8_t i = 0;

// 按键值
uint8_t key = 8;
// 是否更新显示
uint8_t update = 1;
// 距离显示
uint16_t distance = 0;
uint16_t distance_buffer = 0;
// 是否有键按下
uint8_t key_down = 0;
uint8_t key_up = 0;
// 是否暂停计数
uint8_t pause = 0;
// 计数值
uint16_t count = 0;
// 原始值初始化
uint16_t set_dis = 0;
// 是否触发了计数
uint8_t trig = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

void FLASH_Write(uint8_t offset, uint16_t Flash_Val);
uint16_t FLASH_Read(uint8_t offset);

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
	
	tm1638_init();
	VL53L0X_Init();
	
	// Save Last count
	count = (uint16_t)FLASH_Read(1);
	count = count > 9999 ? 0 : count;
	
	HAL_UART_Transmit(&huart1, (uint8_t *)"Ready",5,0xff);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
		key = Read_key();
		
		if(key < 8){
			key_up = key + 1;
		} else {
			if(key_up) {
				if(key_up == 1) {
					set_dis = distance;
				}
				
				if(key_up == 6 && pause) {
					count = 0;
					FLASH_Write(1, count);
				}
				
				if(key_up == 8 && set_dis) {
					pause = ~pause;
				}
				
				update = 1;
				key_up = 0;
			}
		}
		
		if(i%50 == 0) {
			distance = VL53L0X_Distance();
			
			if(distance > 2000){
				distance = 2000;
			}
			
			if(distance != 20 && distance_buffer != distance && !pause) {
				if(set_dis > distance * 1.5) {
					HAL_GPIO_WritePin(led_GPIO_Port, led_Pin, GPIO_PIN_RESET);
					show_dot(1);
					
					trig = 1;
				}
				
				if(trig && set_dis - distance < 50) {
					HAL_GPIO_WritePin(led_GPIO_Port, led_Pin, GPIO_PIN_SET);
					show_dot(0);
					
					if(count++ == 9999) {
						count = 0;
					}
					trig = 0;

					FLASH_Write(1, count);
				}
				
				update = 1;
				distance_buffer = distance;
			}
		}
		
		if(update) {
			if(set_dis) {
				if(pause) {
					show_num(0xffff, count);
				} else {
					show_num(distance, count);
				}
			} else {
				show_num(distance, 0xffff);
			}
			
			update = 0;
		}
		
		if(++i == 200){
			i = 0;
		}
  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSE;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

void FLASH_Write(uint8_t offset, uint16_t Flash_Val) {
  uint32_t address = FLASH_ADD + offset * 1024;

  FLASH_EraseInitTypeDef My_Flash;
  HAL_FLASH_Unlock();

  My_Flash.TypeErase = FLASH_TYPEERASE_PAGES;
  My_Flash.PageAddress = address;
  My_Flash.NbPages = 1;

  uint32_t PageError = 0;
  HAL_FLASHEx_Erase(&My_Flash, &PageError);

  HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, address, Flash_Val);

  HAL_FLASH_Lock();
}

uint16_t FLASH_Read(uint8_t offset) {
  uint32_t address = FLASH_ADD + offset * 1024;

  return *(__IO uint16_t*)( address );
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
