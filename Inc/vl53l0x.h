/**
  ******************************************************************************
  * File Name          : vl53l0x.h
  * Description        : This file contains all the functions prototypes for 
  *                      the vl53l0x
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2020 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __vl53l0x_H
#define __vl53l0x_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Private defines */

#define vl_GPIO_Port GPIOB
#define vl_scl_Pin GPIO_PIN_10
#define vl_sda_Pin GPIO_PIN_11

#define VL53L0X_SCL_Clr() HAL_GPIO_WritePin(vl_GPIO_Port,vl_scl_Pin,GPIO_PIN_RESET)
#define VL53L0X_SCL_Set() HAL_GPIO_WritePin(vl_GPIO_Port,vl_scl_Pin,GPIO_PIN_SET)

#define VL53L0X_SDA_Clr() HAL_GPIO_WritePin(vl_GPIO_Port,vl_sda_Pin,GPIO_PIN_RESET)
#define VL53L0X_SDA_Set() HAL_GPIO_WritePin(vl_GPIO_Port,vl_sda_Pin,GPIO_PIN_SET)

#define VL53L0X_READ_SDA HAL_GPIO_ReadPin(GPIOB,vl_sda_Pin)

#define VL53L0X_REG_IDENTIFICATION_MODEL_ID         0xc0
#define VL53L0X_REG_IDENTIFICATION_REVISION_ID      0xc2
#define VL53L0X_REG_PRE_RANGE_CONFIG_VCSEL_PERIOD   0x50
#define VL53L0X_REG_FINAL_RANGE_CONFIG_VCSEL_PERIOD 0x70
#define VL53L0X_REG_SYSRANGE_START                  0x00
#define VL53L0X_REG_RESULT_INTERRUPT_STATUS         0x13
#define VL53L0X_REG_RESULT_RANGE_STATUS             0x14

/* USER CODE END Private defines */

/* USER CODE BEGIN Prototypes */

void     VL53L0X_Init(void);
void     VL53L0X_sda_out(uint8_t out);
void     VL53L0X_IIC_Start(void);
void     VL53L0X_IIC_Stop(void);
void     VL53L0X_IIC_Wait_Ack(void);
void     VL53L0X_IIC_Ack(uint8_t ack);
void     VL53L0X_Write_IIC_Byte(uint8_t IIC_Byte);
uint8_t  VL53L0X_Read_IIC_Byte(uint8_t ack);
void     VL53L0X_Single_Write(uint8_t addr, uint8_t data);
uint8_t  VL53L0X_Single_Read(uint8_t addr);
uint16_t VL53L0X_Distance(void);

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ pinoutConfig_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
